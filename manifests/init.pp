# Class for R installation with Rstudio and Hadoop data storage
class cran {
    require server
    # Java added for rJava/maxent
    # Disabled do to broken oracle issue
    #require java

    include cran::r
    # Do rstudio manually to prevent installation loop
    #include cran::rstudio
    #include cran::hadoop
}
